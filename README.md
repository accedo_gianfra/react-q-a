# REACT Q&A #

This repo is for answer all the questions about react including all the topics like state management, styling, best practices, resources.

### How to ask a question ###

You can open an issue and explain your doubts or problem or just ask for a opinion.


[ASK a question](https://bitbucket.org/accedo_gianfra/react-q-a/issues/new)


After we collect a good amount of questions we will gather all together and see them and checking if the answers there are correct or we need more explanation

